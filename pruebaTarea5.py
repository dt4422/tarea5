# -*- coding: utf-8 -*-
"""
Created on Sat Mar 18 16:02:34 2017

@author: danit
"""

import unittest
import tarea5


class PruebaTarea5(unittest.TestCase):
    
    def setUp(self):
        super().setUp()
        self.setup()

    def setup(self):
        print("setup:INICICIANDO TEST")
        self.tarea5 = tarea5.Tarea5()

    def tearDown(self):
        print("tearDown: FINALIZANDO TEST")

    def test_1_2_Coincidence_Of_4_Sequences_Of_4_Words(self):
        print("Test1")
        
        self.assertEqual(self.tarea5.percentage_of_totally_conserved_columns('prueba1.txt'), 0.5)
    def test_2_2_Sequences_With_1_Gap_And_2_Sequences_With_0_Gap(self):
        print("Test1")
       
        self.assertEqual(self.tarea5.percentage_of_non_gaps('prueba1.txt'), [1.0, 0.75, 1.0, 0.75])
    if __name__ == "__main__":
        unittest.main()
