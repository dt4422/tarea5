# -*- coding: utf-8 -*-
"""
Created on Tue Mar 14 13:34:20 2017

@author: danit
"""
from collections import Counter


class Tarea5():
    def fasta(fichero):

        identificador = ''
        secuencia = ''

        palabras = []
        for line in open(fichero):  
            if line[0] == '>':

                if identificador != '':
                    palabras.append(secuencia)  
                line = line.split("\n")  
                identificador = line[0].rstrip()  
                secuencia = '' 

            else:
                secuencia = secuencia + line.rstrip()  
        palabras.append(secuencia)
        return palabras  
    
    def percentage_of_totally_conserved_columns(self,fichero):
        lista = Tarea5.fasta(fichero)

        NRepeticiones = []
        dicFrecPos = {}
        cnt = Counter()
        cnt2 = 0
        maximoNumeroComparaciones = 0
        for secuencia1 in lista:
            for secuencia2 in lista:
                if secuencia1 != secuencia2:

                    for i, c1 in enumerate(secuencia1):
                        for j, c2 in enumerate(secuencia2):
                            if c1 == c2 and i == j:
                                NRepeticiones.append(i)

        maximoNumeroComparaciones = len(lista)
        maximoNumeroComparaciones = (maximoNumeroComparaciones * maximoNumeroComparaciones) - maximoNumeroComparaciones

        for ncoincidencias in NRepeticiones:
            cnt[ncoincidencias] += 1  

        dicFrecPos = cnt
        for frec in dicFrecPos.values():
            if frec == maximoNumeroComparaciones:
                cnt2 = 1 + cnt2
        return cnt2/len(secuencia1)


    def percentage_of_non_gaps(self,fichero):
        cnt = 0
        lista = Tarea5.fasta(fichero)
        ngaps = []
        for elemento in lista:
            cnt = 0
            for i, c1 in enumerate(elemento):
                if c1[0] != '-':
                    cnt = cnt + 1
            cnt = cnt / len(elemento)
            ngaps.append(cnt)

        return ngaps
